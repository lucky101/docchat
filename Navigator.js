import React from "react";
import { Text } from "react-native";
import { connect } from "react-redux";
import { createStackNavigator, TabNavigator, TabBarBottom } from "react-navigation";
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
} from "react-navigation-redux-helpers";

import MedicinesScreen from "./screens/MedicinesScreen";
import ProfileScreen from "./screens/ProfileScreen";
import ChatScreen from "./screens/ChatScreen";
import ArchiveScreen from "./screens/ArchiveScreen";

const middleware = createReactNavigationReduxMiddleware("root", state => state.nav);

const tabLabel = label => (
  <Text
    style={{
      color: "black",
      fontSize: 10,
      letterSpacing: 0.12,
      textAlign: "center",
      marginBottom: 4,
    }}
  >
    {label}
  </Text>
);

const HomeNavigator = TabNavigator(
  {
    Medicines: {
      screen: MedicinesScreen,
      navigationOptions: {
        tabBarLabel: tabLabel("Медикаменты"),
      },
    },
    Archive: {
      screen: ArchiveScreen,
      navigationOptions: {
        tabBarLabel: tabLabel("Профайл"),
        // tabBarIcon: ({ focused }) => <Image source={focused ? ordersActiveIcon : ordersIcon} />,
      },
    },
    Chat: {
      screen: ChatScreen,
      navigationOptions: {
        tabBarLabel: tabLabel("Профайл"),
        // tabBarIcon: ({ focused }) => <Image source={focused ? ordersActiveIcon : ordersIcon} />,
      },
    },
    Profile: {
      screen: ProfileScreen,
      navigationOptions: {
        tabBarLabel: tabLabel("Профайл"),
        // tabBarIcon: ({ focused }) => <Image source={focused ? ordersActiveIcon : ordersIcon} />,
      },
    },
  },
  {
    navigationOptions: {
      header: null,
    },
    tabBarOptions: {
      style: {
        backgroundColor: "white",
      },
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: "bottom",
    animationEnabled: false,
    swipeEnabled: false,
  },
);

const RootNavigator = createStackNavigator({
  Main: {
    screen: HomeNavigator,
    navigationOptions: {
      header: null,
    },
  },
});

const initialState = RootNavigator.router.getStateForAction(
  RootNavigator.router.getActionForPathAndParams("Main"),
);

const AppWithNavigationState = reduxifyNavigator(RootNavigator, "root");

const mapStateToProps = state => ({
  state: state.nav,
});

const Navigator = connect(mapStateToProps)(AppWithNavigationState);

export {
  RootNavigator, Navigator, middleware, initialState,
};
