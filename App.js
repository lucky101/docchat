import React from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import reducer from "./reducers/rootReducer";
import { Navigator, middleware } from "./Navigator";

const store = createStore(reducer, applyMiddleware(middleware));

class App extends React.Component {
  state = {};

  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1, marginTop: 20 }}>
          <Navigator />
        </View>
      </Provider>
    );
  }
}

export default App;
