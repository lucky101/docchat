import { combineReducers } from "redux";

import test from "./test";
import nav from "./nav";

const reducer = combineReducers({
  test,
  nav,
});

export default reducer;
