import * as types from "../constants/actionTypes";
import initialState from "./initialState";

export default function (state = initialState.test, action) {
  switch (action.type) {
    case types.TEST_ACTION:
      return { a: action.num };
    default:
      return state;
  }
}
