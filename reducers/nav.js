import { RootNavigator, initialState } from "../Navigator";

export default function (state = initialState, action) {
  console.log(RootNavigator);
  const nextState = RootNavigator.router.getStateForAction(action, state);
  console.log(nextState);
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
