import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";

class ProfileScreen extends Component {
  state = {};

  render() {
    return <View style={{ flex: 1, backgroundColor: "blue" }} />;
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps)(ProfileScreen);
