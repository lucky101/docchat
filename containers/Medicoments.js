import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";

class LoginScreen extends Component {
  render() {
    return <View style={{}} />;
  }
}

const styles = StyleSheet.create({
  no_account_txt: {
    color: "rgba(255,255,255,0.9)",
    backgroundColor: "rgba(0,0,0,0)",
    fontFamily: "Circe",
    fontSize: 15,
  },
  login_wrapper: {
    flex: 1,
    justifyContent: "center",
  },
  login: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  login_btn_txt: {
    color: "white",
    fontFamily: "Futura",
    fontSize: 18,
    fontWeight: "bold",
    backgroundColor: "transparent",
  },
  phone_login_btn: {
    marginBottom: 50,
    backgroundColor: "rgba(32,32,32,0.8)",
    height: 40,
    width: "80%",
    justifyContent: "center",
    alignItems: "center",
  },
  login_btn: {
    marginBottom: 30,
    backgroundColor: "rgb(12,107,243)",
    width: "80%",
    height: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  login_img: {
    resizeMode: "cover",
    width: "100%",
    height: Dimensions.get("window").height + STATUSBAR_HEIGHT,
    marginTop: STATUSBAR_HEIGHT,
  },
  login_spinner: {
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});

export default connect(mapStateToProps)(LoginScreen);
